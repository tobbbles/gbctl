# GiantBombCTL - `gbctl`


## Installing

The best way to install this tool is either build from source or `go get` the repository.

#### Building from source
Once you have the repository checked out, run `go build .` in the working directly. This will produce the `gbctl` binary which you can then move to a desired destination within you $PATH to execute it.

## Usage:

The `--token` must be provided. You can generate an API key [here](https://www.giantbomb.com/api/).

#### Searching
Basic search for games matching the query
```shell script
$ gbctl --token [token] search [query]
```

It's possible to include a list of DLCs for the games by providing the `--dlc` flag.
```shell script
$ gbctl --token [token] search [query] --dlc
```  
