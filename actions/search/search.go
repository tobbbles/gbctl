package search

import (
	"errors"
	"fmt"
	"gbctl/flags"
	"strings"

	"gbctl/remote/giantbomb"
	"gbctl/remote/giantbomb/filter"

	"github.com/jedib0t/go-pretty/table"
	"github.com/urfave/cli"
)

var (
	Command = cli.Command{
		Name:        "search",
		Aliases:     []string{"s"},
		Usage:       "idk this is usage",
		UsageText:   "gbctl search [ape]",
		Description: "search for games matching the given filters",

		Action: Action,
		Flags: []cli.Flag{
			flags.DLCFlag,
		},
	}
)

func Action(ctx *cli.Context) error {
	if len(ctx.Args().First()) == 0 {
		return ErrNoSearchTerm
	}

	client, err := giantbomb.New(flags.APIKey)
	if err != nil {
		return err
	}

	// Manually contruct our search term from all of the arguments.
	term := fmt.Sprintf(
		"%s %s",
		ctx.Args().First(),
		strings.Join(ctx.Args().Tail(), " "),
	)

	search := filter.Name(term)

	// Search for games and include DLC if the flag is passed
	games, err := client.Games(ctx.Bool("dlc"), search)
	if err != nil {
		return err
	}

	if ctx.Bool("dlc") {
		return renderWithDLC(games)
	} else {
		return render(games)
	}
}

func render(games []*giantbomb.Game) error {
	// Format the result into a table
	tw := table.NewWriter()
	tw.AppendHeader(table.Row{"Title", "Release Year", "Platforms"})

	for _, game := range games {
		tw.AppendRow(table.Row{game.Name, game.ExpectedReleaseYear, game.FormatPlatforms()})
	}

	tw.SortBy([]table.SortBy{{Name: "Release Year", Mode: table.Dsc}})

	fmt.Println(tw.Render())

	return nil
}

func renderWithDLC(games []*giantbomb.Game) error {
	// Format the result into a table
	tw := table.NewWriter()
	tw.AppendHeader(table.Row{"Title", "Release Year", "Platforms", "DLCs"})

	for _, game := range games {
		tw.AppendRow(table.Row{game.Name, game.ExpectedReleaseYear, game.FormatPlatforms(), game.FormatDLCs()})
	}

	tw.SortBy([]table.SortBy{{Name: "Release Year", Mode: table.Dsc}})

	fmt.Println(tw.Render())

	return nil
}

// Errors
var (
	ErrNoSearchTerm = errors.New("no search term provided")
)
