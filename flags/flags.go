package flags

import "github.com/urfave/cli"

// Flag Values
var (
	APIKey string

	DLC bool
)

// Flags
var (
	Global = []cli.Flag{
		APIKeyFlag,
	}

	// DLC Specifies whether to include DLC in results
	DLCFlag = cli.BoolFlag{
		Name:        "dlc",
		Usage:       "--dlc",
		Destination: &DLC,
	}

	APIKeyFlag = cli.StringFlag{
		Name:        "token",
		Usage:       "--token [api key from giant bomb]",
		Required:    true,
		Destination: &APIKey,
	}
)
