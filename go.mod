module gbctl

go 1.12

require (
	github.com/go-openapi/strfmt v0.19.2 // indirect
	github.com/jedib0t/go-pretty v4.3.0+incompatible
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/urfave/cli v1.21.0
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect
)
