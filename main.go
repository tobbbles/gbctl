package main

import (
	"gbctl/flags"
	"log"
	"os"

	"gbctl/actions/search"
	"github.com/urfave/cli"
)

func main() {
	app := &cli.App{
		Name:    "gbctl",
		Usage:   "for interacting with the Giant Bomb API",
		Version: "v0.0.0-alpha",
		Commands: []cli.Command{
			search.Command,
		},
		Flags:                 flags.Global,
		HideHelp:              false,
		HideVersion:           false,

		CommandNotFound: nil,
		OnUsageError:    nil,
	}


	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
