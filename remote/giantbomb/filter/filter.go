package filter

import "net/http"

type Filter interface {
	Apply(*http.Request) error
}
