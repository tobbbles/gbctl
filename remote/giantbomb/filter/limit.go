package filter

import (
	"net/http"
	"strconv"
)

type LimitFilter struct {
	limit int
}

// Limit controls how many resources are returned from the API.
func Limit(limit int) *LimitFilter {
	return &LimitFilter{limit}
}

func (f *LimitFilter) Apply(r *http.Request) error {
	q := r.URL.Query()

	q.Add("limit", strconv.Itoa(f.limit))

	r.URL.RawQuery = q.Encode()

	return nil
}
