package filter

import (
	"fmt"
	"net/http"
)

type NameFilter struct {
	title string
}

func Name(name string) *NameFilter {
	return &NameFilter{name}
}

func (f *NameFilter) Apply(r *http.Request) error {
	q := r.URL.Query()

	q.Add("filter", fmt.Sprintf("name:%s", f.title))

	r.URL.RawQuery = q.Encode()

	return nil
}
