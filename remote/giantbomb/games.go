package giantbomb

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"

	"gbctl/remote/giantbomb/filter"
)

var (
	// GamesResource builds the full resource URL
	GamesResource = fmt.Sprintf("%s/games/", Base)

	// DLCResource builds the full resource URL for DLCS
	DLCResource = fmt.Sprintf("%s/game/", Base)
)

type Game struct {
	Aliases                string      `json:"aliases"`
	APIDetailURL           string      `json:"api_detail_url"`
	DateAdded              time.Time   `json:"date_added"`
	DateLastUpdated        time.Time   `json:"date_last_updated"`
	Deck                   string      `json:"deck"`
	Description            string      `json:"description"`
	ExpectedReleaseDay     interface{} `json:"expected_release_day"`
	ExpectedReleaseMonth   interface{} `json:"expected_release_month"`
	ExpectedReleaseQuarter interface{} `json:"expected_release_quarter"`
	ExpectedReleaseYear    interface{} `json:"expected_release_year"`
	GUID                   string      `json:"guid"`
	ID                     int         `json:"id"`
	Image                  *Image      `json:"image"`
	ImageTags              []struct {
		APIDetailURL string `json:"api_detail_url"`
		Name         string `json:"name"`
		Total        int    `json:"total"`
	} `json:"image_tags"`
	Name                string `json:"name"`
	NumberOfUserReviews int    `json:"number_of_user_reviews"`
	OriginalGameRating  []struct {
		APIDetailURL string `json:"api_detail_url"`
		ID           int    `json:"id"`
		Name         string `json:"name"`
	} `json:"original_game_rating"`
	OriginalReleaseDate string      `json:"original_release_date"`
	Platforms           []*Platform `json:"platforms"`
	SiteDetailURL       string      `json:"site_detail_url"`

	// DLC is manually created by us, merging the two data states together
	DLCs []*DLC `json:"-"`
}

// UnmarshalJSON provides a wrapper for unmarshalling the response timestamp format
func (g *Game) UnmarshalJSON(buf []byte) error {
	type Alias Game
	aux := &struct {
		DateAdded       string `json:"date_added"`
		DateLastUpdated string `json:"date_last_updated"`

		*Alias
	}{
		Alias: (*Alias)(g),
	}

	// Unmarshal the byte buffer onto our auxilliary struct
	err := json.Unmarshal(buf, &aux)
	if err != nil {
		return err
	}

	// Manually convert auxilliary timestamps and set them on the top level Game if they exist
	if len(aux.DateAdded) != 0 {
		g.DateAdded, err = time.Parse("2006-01-02 15:04:05", aux.DateAdded)
		if err != nil {
			return err
		}
	}

	if len(aux.DateLastUpdated) != 0 {
		g.DateLastUpdated, err = time.Parse("2006-01-02 15:04:05", aux.DateLastUpdated)
		if err != nil {
			return err
		}
	}

	return nil
}

func (g *Game) FormatPlatforms() string {
	var platforms strings.Builder

	for i, p := range g.Platforms {
		platforms.WriteString(p.Name)

		// Don't append a comma on the final insert!
		if i != len(g.Platforms)-1 {
			platforms.WriteString(", ")
		}
	}

	return platforms.String()
}

func (g *Game) FormatDLCs() string {
	var dlcs strings.Builder

	for i, p := range g.DLCs {
		dlcs.WriteString(p.Name)

		// Don't append a comma on the final insert!
		if i != len(g.Platforms)-1 {
			dlcs.WriteString(", ")
		}
	}

	return dlcs.String()
}

type Platform struct {
	APIDetailURL  string `json:"api_detail_url"`
	ID            int    `json:"id"`
	Name          string `json:"name"`
	SiteDetailURL string `json:"site_detail_url"`
	Abbreviation  string `json:"abbreviation"`
}

type Image struct {
	IconURL        string `json:"icon_url"`
	MediumURL      string `json:"medium_url"`
	ScreenURL      string `json:"screen_url"`
	ScreenLargeURL string `json:"screen_large_url"`
	SmallURL       string `json:"small_url"`
	SuperURL       string `json:"super_url"`
	ThumbURL       string `json:"thumb_url"`
	TinyURL        string `json:"tiny_url"`
	OriginalURL    string `json:"original_url"`
	ImageTags      string `json:"image_tags"`
}

type GameResponse struct {
	Games []*Game `json:"results"`

	*Response
}

// Games will fetch games from the API, adhering to any given filters or search terms
func (c *Client) Games(dlc bool, filters ...filter.Filter) ([]*Game, error) {
	r, err := http.NewRequest(http.MethodGet, GamesResource, nil)
	if err != nil {
		return nil, err
	}

	// Apply filters to the request
	for _, f := range filters {
		if err := f.Apply(r); err != nil {
			return nil, err
		}
	}

	response := &GameResponse{}
	if err := c.do(r, response); err != nil {
		return nil, err
	}

	if response.Error != "OK" {
		return nil, errors.New("fucked it")
	}

	if dlc {
		var wg sync.WaitGroup

		for _, game := range response.Games {
			wg.Add(1)

			go func(game *Game) {
				defer wg.Done()

				dlcs, err := c.DLC(game.GUID)
				if err != nil {
					log.Fatalf("failed whilst collecting game dlcs - %s", err)
				}

				game.DLCs = dlcs
			}(game)
		}

		// Block until all routines have completed
		wg.Wait()
	}

	return response.Games, nil
}

type DLC struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type DLCResponse struct {
	Wrapper *DLCWrapper `json:"results"`

	*Response
}

type DLCWrapper struct {
	DLCs []*DLC `json:"dlcs"`
}

func (c *Client) DLC(guid string) ([]*DLC, error) {
	r, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s%s", DLCResource, guid), nil)
	if err != nil {
		return nil, err
	}

	response := &DLCResponse{}
	if err := c.do(r, response); err != nil {
		return nil, err
	}

	return response.Wrapper.DLCs, nil
}
