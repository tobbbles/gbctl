package giantbomb

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
)

// Base URL of the API.
const Base = "https://www.giantbomb.com/api"

// Response respresents the base response json object we receive from the API. This struct ought to be embedded in
// other response models.
type Response struct {
	Error                string `json:"error"`
	Limit                int    `json:"limit"`
	Offset               int    `json:"offset"`
	NumberOfPageResults  int    `json:"number_of_page_results"`
	NumberOfTotalResults int    `json:"number_of_total_results"`
	StatusCode           int    `json:"status_code"`
	Version              string `json:"version"`
}

// Client provides a set of methods to interact with the GiantBomb API
type Client struct {
	apiKey string

	httpClient *http.Client
}

// New instantiates a GiantBomb client from the given API Key.
func New(apiKey string) (*Client, error) {
	if len(apiKey) == 0 {
		return nil, ErrNoAPIKey
	}

	client := &Client{
		apiKey: apiKey,

		httpClient: &http.Client{
			CheckRedirect: nil,
			Jar:           nil,
			Timeout:       5 * time.Second,
		},
	}

	return client, nil
}

func (c *Client) do(r *http.Request, v interface{}) error {
	q := r.URL.Query()
	q.Add("format", "json")
	q.Add("api_key", c.apiKey)

	r.URL.RawQuery = q.Encode()

	resp, err := c.httpClient.Do(r)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return c.formatStatusError(resp.StatusCode)
	}

	defer resp.Body.Close()
	return json.NewDecoder(resp.Body).Decode(&v)
}

func (c *Client) formatStatusError(code int) error {
	switch code {
	case http.StatusNoContent:
		return errors.New("received no content. double check that your request is valid")

	default:
		return fmt.Errorf("received response code %d", code)
	}
}

// Errors
var (
	ErrNoAPIKey = errors.New("no api key provided to giantbomb client")
)
